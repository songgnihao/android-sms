package me.kullo.android.smsw

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class SMSBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != "") return
        context.startService(Intent(context, SmsForegroundService::class.java).also { it ->
            it.action = "SMSBroadcastReceiver Start"
            it.putExtra("watchAddress", "")
            it.putExtra("token", "")
        })
    }
}