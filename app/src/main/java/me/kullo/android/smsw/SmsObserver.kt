package me.kullo.android.smsw

import android.database.ContentObserver
import android.os.Handler
import android.os.Message


class SmsObserver(handler: Handler) : ContentObserver(handler) {

    private val myHandler: Handler = handler

    override fun onChange(selfChange: Boolean) {
        super.onChange(selfChange)
        //发送事件队列
        Message.obtain(myHandler,0xffa9).sendToTarget()
    }
}