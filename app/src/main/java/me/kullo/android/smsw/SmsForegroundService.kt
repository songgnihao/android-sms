package me.kullo.android.smsw

import android.app.*
import android.content.AsyncQueryHandler
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class SmsForegroundService : Service() {

    private val sdf: SimpleDateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.CHINA)

    companion object {
        const val TAG = "SMSService"
        const val CHANNEL_ID = "me.kullo.android.smsw"
        const val NOTIFICATION_ID = 1
        const val SMS_ALL = "content://sms/"
        const val SMS_INBOX = "content://sms/inbox"
        const val SMS_SEND = "content://sms/sent"
        const val SMS_DRAFT = "content://sms/draft"
        const val SMS_OUTBOX = "content://sms/outbox"
        const val SMS_FAILED = "content://sms/failed"
        const val SMS_QUEUED = "content://sms/queued"
    }

    ///主线程handler
    private lateinit var myHandler: Handler

    ///绑定通知的点击事件
    private lateinit var pendingIntent: PendingIntent

    private var smsObserver: SmsObserver? = null
    private var smsQuery: AsyncQueryHandler? = null

    override fun onCreate() {
        super.onCreate()
        //开启前台服务，在前台服务中进行监听，使用前台服务的好处是可以在开启后离开应用而不被系统终止
        pendingIntent =
            Intent(this, MainActivity::class.java).let {
                PendingIntent.getActivity(this, 0, it, 0)
            }  ///点击通知，打开activity
        createNotificationChannel()//设置通知渠道，必需调用
        //开启前台通知
        startForeground(NOTIFICATION_ID, buildNotification().build())
        //准备handler
        myHandler = object : Handler(Looper.getMainLooper()) {
            override fun handleMessage(msg: Message) {
                super.handleMessage(msg)
                when (msg.what) {
                    0xffa9 -> {
                        Log.i(TAG, "收到短信更新")
                        //收到smsObserver更新消息，读取新短信数据
                        newSmsQueryByPage()
                    }
                    0xffd0 -> {
                        //smsQuery查全部后返回数据
                        handleQueryResult("全部查询完成", msg.obj)
                    }
                    0xffd1 -> {
                        //smsQuery查部分后返回数据
                        handleQueryResult("指定查询完成", msg.obj)
                    }
                    0xffd2 -> {
                        //smsQuery查全部后返回数据
                        handleQueryResult("自动定时查询", msg.obj)
                    }
                }
            }
        }
        //准备SmsContent异步查询处理器，将通过handler返回查询数据
        smsQuery = SmsQuery(myHandler, contentResolver)
        //准备SmsContent监听器
        smsObserver = SmsObserver(myHandler)
        //为true 表示可以同时匹配其派生的Uri
        contentResolver.registerContentObserver(Uri.parse(SMS_ALL), true, smsObserver!!)
        //开启自动轮询闹钟
        startLoopAlarm()
    }

    ///创建循环闹钟，用于定期执行短信内容库检查
    private fun startLoopAlarm() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as? AlarmManager
        //闹钟触发后开启服务
        val alarmIntent = Intent(this, SmsForegroundService::class.java).let { intent ->
            intent.action = "Alarm Start"
            PendingIntent.getService(this, 0, intent, 0)
        }
        alarmManager?.setInexactRepeating(
            AlarmManager.ELAPSED_REALTIME_WAKEUP,
            SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_HALF_HOUR,
            2 * AlarmManager.INTERVAL_HOUR,//两小时触发一次
            alarmIntent
        )
    }

    ///创建通知渠道，android 8 必需
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(
                CHANNEL_ID,
                "SMSW",
                NotificationManager.IMPORTANCE_DEFAULT
            ).let {
                it.description = "A SMS Watcher App"
                (getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager)
                    ?.createNotificationChannel(it)
            }
        }
    }

    ///构建通知栏内容
    private fun buildNotification(
        text: String = "正在监听",
        bigText: String = "正在监听"
    ): NotificationCompat.Builder {
        return NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("短信监听")
            .setContentText(text)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentIntent(pendingIntent)
            .setStyle(NotificationCompat.BigTextStyle().bigText(bigText))//使用大文本样式
            .setPriority(NotificationCompat.PRIORITY_HIGH)
    }

    ///更新通知栏
    private fun updateNotification(title: String = "新短信", smsText: String = "") {
        NotificationManagerCompat.from(this)
            .notify(NOTIFICATION_ID, buildNotification(title, smsText).build())
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand -> ${intent?.action}")
        App.isServiceRunning = true//设置服务运行标志
        //设置监听对象，没有新传值，则用内存中的
        watchAddress = intent?.getStringExtra("watchAddress") ?: watchAddress
        token = intent?.getStringExtra("token") ?: token
        Log.i(TAG, "watchAddress -> $watchAddress")
        Log.i(TAG, "token -> $token")
        //进行内容扫描
        updateNotification("收到Start", "正在查询")
        when (intent?.action) {
            "MainActivity Start" -> allNewSmsQuery()
            "SMSBroadcastReceiver Start" -> newSmsQueryByPage()
            else -> allSmsAutoQuery()
        }
        return START_STICKY_COMPATIBILITY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        App.isServiceRunning = false//结束服务运行标志
    }

    override fun onDestroy() {
        App.isServiceRunning = false//结束服务运行标志
        //清空队列
        myHandler.removeMessages(0xffa9)
        myHandler.removeMessages(0xffd0)
        myHandler.removeMessages(0xffd1)
        //移除监听
        smsObserver?.let { contentResolver.unregisterContentObserver(it) }
        stopForeground(true)
        smsObserver = null
        //停止查询
        smsQuery?.removeCallbacksAndMessages(0)
        smsQuery?.cancelOperation(0)
        smsQuery = null
        //协程内容取消
        scope.cancel()
    }

    private val queryColumn = arrayOf(
        "_id", "address", "person",
        "body", "date", "type"
    )

    ///要监听的短信地址
    private var watchAddress = ""
    private var token = ""

    ///全部查询，将查出全部消息，将通过0xffd0消息返回列表数据
    private fun allNewSmsQuery() {
        if (watchAddress.isEmpty()) return
        smsQuery?.startQuery(
            0xffd0, null, Uri.parse(SMS_ALL), queryColumn, "address=?",
            arrayOf(watchAddress), "date desc"
        )
    }

    ///按指定分页查询出新的消息，将通过0xffd1消息返回列表数据，默认只查最近的一条
    private fun newSmsQueryByPage(page: Int = 0, size: Int = 1) {
        if (watchAddress.isEmpty()) return
        smsQuery?.startQuery(
            0xffd1, null, Uri.parse(SMS_ALL), queryColumn, "address=?",
            arrayOf(watchAddress), "date desc limit ${page * size},$size"
        )
    }

    ///全部查询，将查出全部消息，将通过0xffd2消息返回列表数据
    private fun allSmsAutoQuery() {
        if (watchAddress.isEmpty()) return
        smsQuery?.startQuery(
            0xffd2, null, Uri.parse(SMS_ALL), queryColumn, "address=?",
            arrayOf(watchAddress), "date desc"
        )
    }

    //定义一个协程作用域
    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    ///处理查询出的短信
    private fun handleQueryResult(title: String, obj: Any) {
        Log.i(TAG, "$title -> $obj")
        val list = obj as List<SmsEntity>
        //更新内容到通知
        updateNotification("查询结束", "$title -> 通知时间：${sdf.format(Date())}")
        //更新内容到数据库
        scope.launch(Dispatchers.IO) {
            coroutineScope {
                //插入数据库，自动去重
                App.smsDb.insertAll(list)
                //取出未上传内容进行上传
                val noUpList = App.smsDb.loadAllNoUp()
                Log.i(TAG, "待上传信息：${noUpList.size}条")
                val upSucList = mutableListOf<SmsEntity>()
                //上传服务器
                noUpList.forEach {
                    val r = sendToService(it)
                    if (r != null && r.hasUp == 1)
                        upSucList.add(r)//上传成功，添加到数据库更新列表
                }
                if (upSucList.isNotEmpty()) {
                    App.smsDb.upDate(upSucList)   //上传完成后本地状态更新
                }
            }
        }
    }

    private suspend fun sendToService(sms: SmsEntity): SmsEntity? {
        //没有token则不上传
        if (token.isEmpty()) return null
        val v = NetApi.upload(token, sms.body)
        if (v.code == -1) return null
        return sms.also {
            it.hasUp = if (v.code == 1000) 1 else 0
            it.upMsg = "${v.code}|${v.message}|${v.data}"
        }
    }
}