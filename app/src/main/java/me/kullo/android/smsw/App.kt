package me.kullo.android.smsw

import android.app.Application
import androidx.room.Room

class App : Application() {

    companion object {
        lateinit var smsDb: SmsDao

        var isServiceRunning = false
    }

    override fun onCreate() {
        super.onCreate()
        smsDb = Room.databaseBuilder(
            this,
            AppDatabase::class.java, "smsDB"
        ).allowMainThreadQueries().build().smsDao()
    }
}