package me.kullo.android.smsw

import android.content.AsyncQueryHandler
import android.content.ContentResolver
import android.database.Cursor
import android.database.sqlite.SQLiteException
import android.os.Handler
import android.os.Message
import android.util.Log

class SmsQuery(handler: Handler, cr: ContentResolver) : AsyncQueryHandler(cr) {

    companion object {
        const val TAG = "SmsQuery"
    }

    private val myHandler: Handler = handler

    override fun onQueryComplete(token: Int, cookie: Any?, cursor: Cursor?) {
        super.onQueryComplete(token, cookie, cursor)
        Log.i(TAG, "$token -> 查询完成")
        Message.obtain(myHandler, token, readSMSContent(cursor)).sendToTarget()
    }

    private fun readSMSContent(cursor: Cursor?): List<SmsEntity> {
        val smsList = mutableListOf<SmsEntity>()
        try {
            // 读出内容
            cursor?.let {
                val iD: Int = it.getColumnIndex("_id")
                val iAddress: Int = it.getColumnIndex("address")
                val iPerson: Int = it.getColumnIndex("person")
                val iBody: Int = it.getColumnIndex("body")
                val iDate: Int = it.getColumnIndex("date")
                val iType: Int = it.getColumnIndex("type")
                if (it.moveToFirst()) {
                    do {
                        smsList.add(
                            SmsEntity(
                                it.getInt(iD),
                                it.getString(iAddress),
                                it.getInt(iPerson),
                                it.getString(iBody),
                                it.getLong(iDate),
                                it.getInt(iType),
                                0, 0, ""
                            )
                        )
                    } while (it.moveToNext())
                    it.close()
                } else {
                    Log.i(TAG, "no result!")
                }
            }
        } catch (ex: SQLiteException) {
            Log.d(TAG, "${ex.message}")
        }
        return smsList
    }
}