package me.kullo.android.smsw

import android.Manifest
import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<View>(R.id.start).setOnClickListener {
            //权限检查
            val needPermissionList = mutableListOf<String>()
            if (ContextCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.READ_SMS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                needPermissionList.add(Manifest.permission.READ_SMS)
            }
            if (ContextCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.RECEIVE_SMS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                needPermissionList.add(Manifest.permission.RECEIVE_SMS)
            }
            if (needPermissionList.isNotEmpty())
                ActivityCompat.requestPermissions(
                    this@MainActivity,
                    needPermissionList.toTypedArray(),
                    0x000f
                )
            else
                start()//拥有权限，直接开始
        }
        findViewById<View>(R.id.button).setOnClickListener {
            //开启新页面，查看已处理内容
            startActivity(Intent(this, MsmShowActivity::class.java))
        }
        //设置host
        val sp = getPreferences(MODE_PRIVATE)
        findViewById<EditText>(R.id.et_host).setText(sp.getString("host", ""))
        findViewById<EditText>(R.id.et_code).setText(sp.getString("code", ""))
    }

    override fun onResume() {
        super.onResume()
        //每次进入检查应用关联前台服务运行状态
        val v = isServiceRunning()
        if (v)
            setStartButton(false, "服务已启动")
        else
            setStartButton(true, "点击开始监听")
    }

    private fun setStartButton(enableClick: Boolean = false, text: String = "") {
        findViewById<Button>(R.id.start)?.let {
            it.isClickable = enableClick
            it.setText(text)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            0x000f -> if (grantResults.isNotEmpty() && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                start()  //获得权限，开始
            } else {
                Toast.makeText(this@MainActivity, "需要授予全部权限", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun start() {
        val host = findViewById<EditText>(R.id.et_host).text?.toString() ?: ""
        val code = findViewById<EditText>(R.id.et_code).text?.toString() ?: ""
        if (host.isEmpty() || code.isEmpty()) {
            Toast.makeText(this@MainActivity, "先设置Host和Code", Toast.LENGTH_SHORT).show()
            return
        }
        if (!(host.startsWith("http") && host.endsWith("/"))) {
            Toast.makeText(
                this@MainActivity,
                "Host格式为http(s)://XXX.XXX(:0000)/",
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        //保存到sp
        getPreferences(MODE_PRIVATE).edit()?.also {
            it.putString("host", host)
            it.putString("code", code)
        }?.apply()
        //登录
        setStartButton(false, "正在启动")  //设置按钮显示服务状态
        NetApi.BASE_URL = host
        lifecycleScope.launch(Dispatchers.IO) {
            val v = NetApi.getToken(code)
            withContext(Dispatchers.Main) {
                if (v.code != -1 && v.data != null) {//启动服务
                    startService(v.data.token, v.data.phone)
                } else {
                    Toast.makeText(this@MainActivity, v.message, Toast.LENGTH_SHORT).show()
                    setStartButton(true, "点击开始监听")  //设置按钮显示服务状态
                }
            }
        }
    }

    ///开始监听
    private fun startService(token: String?, address: String?) {
        Intent(this, SmsForegroundService::class.java).also { it ->
            it.action = "MainActivity Start"
            it.putExtra("watchAddress", address)
//            it.putExtra("watchAddress", "10086033")
            it.putExtra("token", token)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForegroundService(it)//启动前台服务
            } else {
                startService(it)
            }
        }
        Toast.makeText(this@MainActivity, "已启动", Toast.LENGTH_SHORT).show()
        setStartButton(false, "服务已启动")  //设置按钮显示服务状态
    }

    ///检查前台服务是否运行
    private fun isServiceRunning(): Boolean {
        return App.isServiceRunning
    }
}