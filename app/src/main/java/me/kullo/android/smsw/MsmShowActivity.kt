package me.kullo.android.smsw

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class MsmShowActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_msm_show)
        //列表展示全部内容
        lifecycleScope.launch(Dispatchers.IO) {
            val list = App.smsDb.loadAllNoSh()
            withContext(Dispatchers.Main) {
                //切换回主线程，更新ui
                findViewById<RecyclerView>(R.id.listView).also {
                    it.layoutManager = LinearLayoutManager(this@MsmShowActivity)
                    it.adapter = SmsAdapter(list)
                    title = "共${list.size}条"
                }
            }
        }
    }

    class SmsAdapter(private val dataSet: List<SmsEntity>) :
        RecyclerView.Adapter<SmsAdapter.ViewHolder>() {

        private val sdf: SimpleDateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.CHINA)

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tvId: TextView = view.findViewById(R.id.tv_id)
            val tvTime: TextView = view.findViewById(R.id.tv_time)
            val tvAddress: TextView = view.findViewById(R.id.tv_address)
            val tvBody: TextView = view.findViewById(R.id.tv_body)
            val tvUpMsg: TextView = view.findViewById(R.id.tv_up_msg)
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.list_item_sms, viewGroup, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
            viewHolder.tvId.text = "${dataSet[position].rId}"
            viewHolder.tvTime.text =
                "${dataSet[position].type} | ${sdf.format(Date(dataSet[position].date))}"
            viewHolder.tvAddress.text = "${dataSet[position].address}"
            viewHolder.tvBody.text = "${dataSet[position].body}"
            viewHolder.tvUpMsg.text = "${dataSet[position].upMsg}"
            if (dataSet[position].hasUp == 1)
                viewHolder.tvUpMsg.setTextColor(android.graphics.Color.GREEN)
        }

        override fun getItemCount() = dataSet.size
    }
}