package me.kullo.android.smsw

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST
import java.lang.Exception
import java.util.concurrent.TimeoutException

object NetApi {
    var BASE_URL = ""

    suspend fun getToken(code: String): BaseResult<TokenBean> {
        if (BASE_URL.isEmpty()) return BaseResult(-1, "接口参数不存在", TokenBean.nil)
        return try {
            Api.get(BASE_URL).getToken(code)
        } catch (e: Exception) {
            e.printStackTrace()
            when (e) {
                is TimeoutException -> BaseResult<TokenBean>(-1, "请求超时", TokenBean.nil)
                else -> BaseResult(-1, "网络请求出错-》${e.message}", TokenBean.nil)
            }
        }
    }

    suspend fun upload(
        token: String, body: String
    ): BaseResult<String> {
        if (BASE_URL.isEmpty()) return BaseResult(-1, "接口参数不存在", "")
        return try {
            Api.get(BASE_URL).upload(token, body)
        } catch (e: Exception) {
            e.printStackTrace()
            BaseResult(-1, "网络请求出错-》${e.message}", "")
        }
    }
}

private interface Api {
    companion object {
        private var api: Api? = null

        private var host = ""

        fun get(baseUrl: String): Api {
            if (api != null && host == baseUrl) return api!!
            val httpLoggingInterceptor =
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val client = OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            api = retrofit.create(Api::class.java)
            return api!!
        }
    }

    /**
     * 获取token等内容
     */
    @FormUrlEncoded
    @POST("supplier/card/auth")
    suspend fun getToken(@Field("code") code: String): BaseResult<TokenBean>

    /**
     * 上传内容
     */
    @FormUrlEncoded
    @POST("pay/push")
    suspend fun upload(
        @Header("token") token: String,
        @Field("content") body: String
    ): BaseResult<String>
}

data class BaseResult<T>(val code: Int = 0, val message: String = "", val data: T)

data class TokenBean(val phone: String = "", val template: String = "", val token: String = "") {
    companion object {
        val nil = TokenBean()
    }
}
