package me.kullo.android.smsw

import androidx.room.*

@Entity(tableName = "sms_entity")
data class SmsEntity(
    @PrimaryKey val rId: Int,//原内容索引，用于重复性判断
    val address: String,
    val person: Int,
    val body: String,
    val date: Long,
    val type: Int,
    var hasUp: Int,//是否已上传
    var hasSh: Int,//是否已展示过
    var upMsg: String//上传时接口返回内容
)

@Dao
interface SmsDao {
    //插入内容，重复则忽略
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(smsE: SmsEntity)

    //插入内容，重复则忽略
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(smsEs: List<SmsEntity>)

    //查询全部未展示内容
    @Query("SELECT * FROM sms_entity WHERE hasSh = 0 ORDER BY date DESC")
    suspend fun loadAllNoSh(): List<SmsEntity>

    //查询全部已上传内容
    @Query("SELECT * FROM sms_entity WHERE hasUp = 1 ORDER BY date DESC")
    suspend fun loadAllUp(): List<SmsEntity>

    //查询全部未上传内容
    @Query("SELECT * FROM sms_entity WHERE hasUp = 0 ORDER BY date DESC")
    suspend fun loadAllNoUp(): List<SmsEntity>

    //更新指定内容
    @Update()
    suspend fun upDate(smsEs: List<SmsEntity>)

    //更新指定内容
    @Update()
    suspend fun upDate(smsEs: SmsEntity)
}


@Database(entities = [SmsEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun smsDao(): SmsDao
}


